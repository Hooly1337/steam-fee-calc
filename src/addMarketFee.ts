import calcMarketFee from  "./calcMarketFee";


export function addMarketFee(amount: number): number {
    return calcMarketFee(amount*100).amount/100;
}

export default addMarketFee;



