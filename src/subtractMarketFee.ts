import calcMarketFee from "./calcMarketFee";
import { WalletInfo } from "./calcMarketFee";

export function subtractMarketFee(amount: number): number {
    amount = amount*100;

    let iterations = 0;
    let estimatedAmountOfWalletFundsReceivedByOtherParty = parseInt(String(amount / (WalletInfo.wallet_fee_percent + WalletInfo.publisher_fee + 1)));
    let everUndershot = false;
    let fees = calcMarketFee(estimatedAmountOfWalletFundsReceivedByOtherParty);
    while ( fees.amount != amount && iterations < 10 )
    {
        if ( fees.amount > amount )
        {
            if ( everUndershot )
            {
                fees = calcMarketFee( estimatedAmountOfWalletFundsReceivedByOtherParty - 1);
                fees.steamFee += ( amount - fees.amount );
                fees.fees += ( amount - fees.amount );
                fees.amount = amount;
                break;
            }
            else
            {
                estimatedAmountOfWalletFundsReceivedByOtherParty--;
            }
        }
        else
        {
            everUndershot = true;
            estimatedAmountOfWalletFundsReceivedByOtherParty++;
        }

        fees = calcMarketFee( estimatedAmountOfWalletFundsReceivedByOtherParty );
        iterations++;
    }
    return (fees.amount - fees.fees)/100;
}

export default subtractMarketFee;