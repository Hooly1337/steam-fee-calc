export enum WalletInfo{
    wallet_fee_percent=0.05,
    wallet_fee_minimum=1,
    wallet_fee_base=0,
    publisher_fee=0.10
}

export function calcMarketFee(amount: number): { amount: number, steamFee: number, publisherFee: number, fees: number }
{
    const steamFee = parseInt( String(Math.floor(Math.max(amount * WalletInfo.wallet_fee_percent, WalletInfo.wallet_fee_minimum) + WalletInfo.wallet_fee_base)) );
    const publisherFee = parseInt( String(Math.floor(WalletInfo.publisher_fee > 0 ? Math.max(amount * WalletInfo.publisher_fee, 1) : 0)) );
    const amountWithFee = amount + steamFee + publisherFee;
    return { amount: amountWithFee, publisherFee: publisherFee, steamFee: steamFee, fees: publisherFee + steamFee }
}

export default calcMarketFee;