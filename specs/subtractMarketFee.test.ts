import subtractMarketFee from "../src/subtractMarketFee";


describe('subtractMarketFee test suit', () => {
    it('should calc amount without comms right 1', () => {
        expect(subtractMarketFee(99)).toBe(86.09)
    })

    it('should calc amount without comms right 2', () => {
        expect(subtractMarketFee(123)).toBe(106.97)
    })

    it('should calc amount without comms right 3', () => {
        expect(subtractMarketFee(45.43)).toBe(39.51)
    })

    it('should calc amount without comms right 4', () => {
        expect(subtractMarketFee(0.43)).toBe(0.39)
    })

    it('should calc amount without comms right 5', () => {
        expect(subtractMarketFee(1003.98)).toBe(873.03 )
    })
})