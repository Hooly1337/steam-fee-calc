import addMarketFee from "../src/addMarketFee"

describe("addMarketFee test suit", () => {
    it("should calc right 1", () => {
        expect(addMarketFee(100)).toBe(115)
    })

    it("should calc right 2", () => {
        expect(addMarketFee(10)).toBe(11.5)
    })

    it("should calc right 3", () => {
        expect(addMarketFee(5)).toBe(5.75)
    })

    it("should calc right 4", () => {
        expect(addMarketFee(1)).toBe(1.15)
    })

    it("should calc right 5", () => {
        expect(addMarketFee(337.5)).toBe(388.12)
    })

    it("should calc right 6", () => {
        expect(addMarketFee(113.43)).toBe(130.44)
    })

    it("should calc right 7", () => {
        expect(addMarketFee(2141.49)).toBe(2462.70)
    })
})