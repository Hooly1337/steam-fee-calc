module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    testPathIgnorePatterns: ["/node_modules/", "/include/"],
    testTimeout: 99999999
};